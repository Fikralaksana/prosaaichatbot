from difflib import SequenceMatcher

class Chatbot():
    def __init__(self,qaArray):
        self.arr=qaArray
        self.qa={}
        for i in range(len(self.arr))[::2]:
            self.qa[(self.arr[i]).lower()]=self.arr[i+1].lower()
    def get_response(self,question):
        question=question.lower()
        for i in self.qa:
            if SequenceMatcher(None, question, i).ratio()>0.5:
                return self.qa[i]
        return question
            


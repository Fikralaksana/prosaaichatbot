from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .bot import Chatbot
from .datasetText import DATASET
from functools import wraps
import json
# Create your views here.

def authorize(func):
    @wraps(func)
    def wrapper(request,*args, **kwargs):
        if request.headers.get('Authorization')!='code':
            return Response('auth gagal')
        else :
            return func(request,*args,*kwargs)

    return wrapper

@api_view(['GET'])
@authorize
def get_response(request):
    question=(request.GET.get('question'))
    bot=Chatbot(DATASET)
    ans=bot.get_response(question)
    resp={}
    resp[question]=ans

    return Response(resp)
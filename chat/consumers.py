import json,random,string
import requests_async as requests
from channels.generic.websocket import WebsocketConsumer,AsyncWebsocketConsumer
from asgiref.sync import async_to_sync


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        user_id=self.room_name
        await self.accept()

        await self.send(text_data=json.dumps({
            'code': 1,
            'message': user_id,
        }))

        await self.send(text_data=json.dumps({
            'sender':'Server',
            'code': 0,
            'message': 'Selamat Datang di test prosa.ai'
        }))


    async def disconnect(self, close_code):
        pass

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json.get('message')
        code = text_data_json.get('code')
        sender = text_data_json.get('sender')

        if code==0:
            await self.send(text_data=json.dumps({
                'type': 'chat_message',
                'code': code,
                'sender': sender, 
                'message': message
                }))
            await self.server_message({
                'type': 'chat_message',
                'code': code,
                'sender': sender, 
                'message': message
                })
        elif code==1:
            await self.send(text_data=json.dumps({
                'type': 'chat_message',
                'code': code,
                'sender': sender, 
                'message': message
                }))
            if message=="CLOSE_SOCKET":
                await self.close()
    

    async def server_message(self,event):
        welcome = event.get('welcome')
        params=event.get('message')
        message= await requests.get('http://127.0.0.1:8000/api?question=%s'%params,headers={'Authorization':'code'})
        message=(message.json().get(params))
        if welcome:
            await self.send(text_data=json.dumps({
                'sender': 'Server',
                'message': welcome,
                'code': 0
            }))
        elif message:
            await self.send(text_data=json.dumps({
                'sender': 'Server',
                'message': message,
                'code': 0
            }))



    async def sys_message(self, event):
        message = event.get('message')
        code=event.get('code')

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'code': code
        }))

    